
////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Relative build directory location
 */
val rootBuildOutputDir = "../__build-output"

////////////////////////////////////////////////////////////////////////////////////////////////////

// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    // https://medium.com/android-dev-hacks/kotlin-dsl-gradle-scripts-in-android-made-easy-b8e2991e2ba
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath(Gradle.androidPlugin)
        classpath(Gradle.kotlinPlugin)

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle.kts files
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
    }

    buildDir = File("$rootDir/$rootBuildOutputDir/${project.name}")
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
