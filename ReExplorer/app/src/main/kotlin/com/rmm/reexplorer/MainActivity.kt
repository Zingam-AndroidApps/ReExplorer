package com.rmm.reexplorer

import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Face
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.rmm.rexplorer.ui.theme.ReExplorerTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ReExplorerTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Greeting(listOf("Android", "Jetpack", "Compose"))
                }
            }
        }
    }
}

@Composable
fun Greeting(itemsList: List<String>) {
    val configuration = LocalConfiguration.current
    val orientation = when (configuration.orientation) {
        Configuration.ORIENTATION_PORTRAIT ->
            "portrait"
        Configuration.ORIENTATION_LANDSCAPE ->
            "landscape"
        Configuration.ORIENTATION_UNDEFINED ->
            "undefined"
        else ->
            "Invalid: orientation"
    }
    val navigation = when (configuration.navigation) {
        Configuration.NAVIGATION_DPAD ->
            "dpad"
        Configuration.NAVIGATION_NONAV ->
            "nonav"
        Configuration.NAVIGATION_TRACKBALL ->
            "trackball"
        Configuration.NAVIGATION_UNDEFINED ->
            "undefined"
        Configuration.NAVIGATION_WHEEL ->
            "wheel"
        else ->
            "Invalid: NAVIGATION"
    }
    val uiModeType = when (configuration.uiMode and Configuration.UI_MODE_TYPE_MASK) {
        Configuration.UI_MODE_TYPE_APPLIANCE ->
            "appliance"
        Configuration.UI_MODE_TYPE_CAR ->
            "car"
        Configuration.UI_MODE_TYPE_DESK ->
            "desk"
        Configuration.UI_MODE_TYPE_NORMAL ->
            "normal"
        Configuration.UI_MODE_TYPE_TELEVISION ->
            "television"
        Configuration.UI_MODE_TYPE_UNDEFINED ->
            "undefined"
        Configuration.UI_MODE_TYPE_VR_HEADSET ->
            "vr headset"
        Configuration.UI_MODE_TYPE_WATCH ->
            "watch"
        else ->
            "Invalid: UI_MODE_TYPE"
    }
    val uiModeNight = when (configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
        Configuration.UI_MODE_NIGHT_NO ->
            "day mode"
        Configuration.UI_MODE_NIGHT_YES ->
            "night mode"
        Configuration.UI_MODE_NIGHT_UNDEFINED ->
            "undefined"
        else ->
            "Invalid: UI_MODE_NIGHT"
    }
    val screenLayoutLayoutDir =
        when (configuration.screenLayout and Configuration.SCREENLAYOUT_LAYOUTDIR_MASK) {
            Configuration.SCREENLAYOUT_LAYOUTDIR_LTR ->
                "ltr"
            Configuration.SCREENLAYOUT_LAYOUTDIR_RTL ->
                "rtl"
            Configuration.SCREENLAYOUT_LAYOUTDIR_SHIFT ->
                "shift"
            Configuration.SCREENLAYOUT_LAYOUTDIR_UNDEFINED ->
                "undefined"
            else ->
                "Invalid: SCREENLAYOUT_LAYOUTDIR"
        }
    val screenLayoutLayoutLong =
        when (configuration.screenLayout and Configuration.SCREENLAYOUT_LONG_MASK) {
            Configuration.SCREENLAYOUT_LONG_NO ->
                "no"
            Configuration.SCREENLAYOUT_LONG_UNDEFINED ->
                "undefined"
            Configuration.SCREENLAYOUT_LONG_YES ->
                "yes"
            else ->
                "Invalid: SCREENLAYOUT_LONG"
        }
    val screenLayoutLayoutRound =
        when (configuration.screenLayout and Configuration.SCREENLAYOUT_ROUND_MASK) {
            Configuration.SCREENLAYOUT_ROUND_NO ->
                "no"
            Configuration.SCREENLAYOUT_ROUND_UNDEFINED ->
                "undefined"
            Configuration.SCREENLAYOUT_ROUND_YES ->
                "yes"
            else ->
                "Invalid: SCREENLAYOUT_ROUND"
        }
    val screenLayoutSize =
        when (configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK) {
            Configuration.SCREENLAYOUT_SIZE_LARGE ->
                "large"
            Configuration.SCREENLAYOUT_SIZE_NORMAL ->
                "normal"
            Configuration.SCREENLAYOUT_SIZE_SMALL ->
                "small"
            Configuration.SCREENLAYOUT_SIZE_UNDEFINED ->
                "undefined"
            Configuration.SCREENLAYOUT_SIZE_XLARGE ->
                "xlarge"
            else ->
                "Invalid: SCREENLAYOUT_SIZE"
        }

    LazyColumn(modifier = Modifier.fillMaxSize()) {
        val allItemsList = itemsList + listOf(
            orientation,
            navigation,
            screenLayoutLayoutDir,
            screenLayoutLayoutLong,
            screenLayoutLayoutRound,
            screenLayoutSize,
            uiModeType,
            uiModeNight,
        )

        items(allItemsList) { name ->
            Row {
                Icon(
                    Icons.Sharp.Face,
                    contentDescription = "Icon name",
                    modifier = Modifier
                        .height(50.dp)
                        .width(50.dp)
                )
                Text(
                    text = "Hello, $name!",
                    modifier = Modifier.align(Alignment.CenterVertically)
                )
            }
        }
    }
}

@Preview(
    group = "Home Mobile",
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Preview(
    group = "Home Mobile",
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES
)
@Preview(
    group = "Home Large",
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    device = Devices.PIXEL_C
)
@Preview(
    group = "Home Large",
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    device = Devices.PIXEL_C
)
@Composable
fun DefaultPreview() {
    ReExplorerTheme {
        Greeting(listOf("Android"))
    }
}
