
object Gradle {

    const val androidPlugin = "com.android.tools.build:gradle:${Versions.gradle}"

    const val kotlinPlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"

}

object Libs {

    object AndroidX {

        object Activity {

            const val activityCompose = "androidx.activity:activity-compose:${Versions.activityCompose}"

        }

        const val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"

        object Compose {

            const val ui = "androidx.compose.ui:ui:${Versions.compose}"
            const val material = "androidx.compose.material:material:${Versions.compose}"

            const val uiTooling = "androidx.compose.ui:ui-tooling:${Versions.compose}"
            const val uiToolingPreview = "androidx.compose.ui:ui-tooling-preview:${Versions.compose}"

            const val uiTest = "androidx.compose.ui:ui-test-junit4:${Versions.compose}"

        }

        /**
         * The Core KTX module provides extensions for common libraries that are part of the Android framework.
         */
        const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"

        object Lifecycle {

            const val runtimeKtx =  "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycleRuntimeKtx}"

        }

        object Test {

            object Ext {
                const val junit = "androidx.test.ext:junit-ktx:${Versions.androidxJuint}"
            }

            const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoCore}"

        }
    }

    const val material = "com.google.android.material:material:${Versions.material}"

    object JUnit {

        const val junit = "junit:junit:${Versions.junit}"

    }

}
