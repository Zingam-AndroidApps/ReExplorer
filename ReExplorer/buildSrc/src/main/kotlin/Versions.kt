
object Versions {

    // App level
    const val gradle = "7.0.2"
    const val kotlin = "1.5.10"

    // Libraries
    const val compose = "1.0.0"
    const val coreKtx = "1.6.0"
    const val appcompat = "1.3.1"
    const val material = "1.4.0"
    const val lifecycleRuntimeKtx = "2.3.1"
    const val activityCompose = "1.3.1"
    const val junit = "4.1"
    const val androidxJuint = "1.1.3"
    const val espressoCore = "3.4.0"

}
