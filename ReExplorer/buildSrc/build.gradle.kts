import org.gradle.kotlin.dsl.`kotlin-dsl`

////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Relative build directory location
 */
val rootBuildOutputDir = "../../__build-output"

////////////////////////////////////////////////////////////////////////////////////////////////////

plugins {
    `kotlin-dsl`
}

repositories {
    google()
    mavenCentral()
}

allprojects {
    buildDir = File("$rootDir/$rootBuildOutputDir/${project.name}")
}
